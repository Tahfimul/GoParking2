package com.example.home_pc2.goparking;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter {

    private GoogleMap mMap;
    private UiSettings mUiSettings;
    private boolean state;
    Switch marker, statusIndicator;
    private static final LatLng BRISBANE = new LatLng(-27.47093, 153.0235);
    private static final LatLng BRISBAN = new LatLng(-26, 156);
    static String Status, User, State;
    String locations = "Brisbane";
    Marker mark;
    int i = 0, j = 0, k = 0;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference main = database.getReference("/");
    DatabaseReference mainLocation = main.child("Location");
    DatabaseReference location = mainLocation.child(locations);
    DatabaseReference status = location.child("Status");
    DatabaseReference indicator = location.child("CanvasState");
    DatabaseReference user = location.child("User");
    LocationManager locationManager ;
    boolean GpsStatus ;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        state = true;
        marker = (Switch) findViewById(R.id.marker);
        statusIndicator = (Switch) findViewById(R.id.status);
        Status = "None";
        State = "";
        User = "N/A";
        context = getApplicationContext();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mUiSettings = mMap.getUiSettings();

        // Keep the UI Settings state in sync with the checkboxes.
        mUiSettings.setZoomControlsEnabled(state);
        mUiSettings.setMyLocationButtonEnabled(state);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());

        CheckGpsStatus() ;

        if(GpsStatus == true)
        {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mUiSettings.setMyLocationButtonEnabled(state);
            mMap.setMyLocationEnabled(state);
            }
        }else {
            mUiSettings.setMyLocationButtonEnabled(false);
            mMap.setMyLocationEnabled(false);
            Intent intent = new Intent(this, DisplayMessageActivity.class);
            startActivity(intent);
        }

        status.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if(value != null) {
                    Status = value;
                }
                if(State == "true"){
                    if(mark.isVisible()) {
                        mark.setVisible(false);
                    }
                }
                switch (Status) {
                    case "Occupied":
                        mark = mMap.addMarker(new MarkerOptions()
                                .position(BRISBAN)
                                .title(Status)
                                .snippet(Status)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_occupied)));
                        mark.setVisible(true);
                        break;
                    case "Unoccupied":
                        mark=mMap.addMarker(new MarkerOptions()
                                .position(BRISBAN)
                                .title(Status)
                                .snippet(Status)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_unoccupied)));
                        mark.setVisible(true);
                        break;
                    default:
                        Toast.makeText(MapsActivity.this, "Status info not available", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        indicator.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String state = dataSnapshot.getValue(String.class);
                if(state != null){
                    State = state;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String user = dataSnapshot.getValue(String.class);
                if (user != null){
                    User = user;
                }
                switch (User){
                    case "None":
                        marker.setText("");
                        marker.setEnabled(true);
                        marker.setClickable(true);
                        break;
                    case "User1":
                        marker.setText("");
                        marker.setEnabled(true);
                        marker.setClickable(true);
                        break;
                    default:
                        marker.setEnabled(false);
                        marker.setText("Someone else is parked here!");
                        break;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        mainLocation.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(locations)){
                    marker.setEnabled(true);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        marker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(State == ""){
                    mainLocation.child(locations).child("CanvasCounter").setValue("1");
                    mainLocation.child(locations).child("Status").setValue("Occupied");
                    mainLocation.child(locations).child("User").setValue("None");
                }

                switch (User){
                    case "None":
                        user.setValue("User1");
                        i++;
                        j++;
                        if(j == 1){
                            status.setValue("Occupied");
                        }
                        if(j > 1){
                            j = 0;
                            status.setValue("Unoccupied");
                        }
                        State = "true";
                        indicator.setValue(State);
                        break;
                    case "User1":
                        user.setValue("None");
                        i++;
                        j++;
                        if(j == 1){
                            status.setValue("Occupied");
                        }
                        if(j > 1){
                            j = 0;
                            status.setValue("Unoccupied");
                        }
                        State = "true";
                        indicator.setValue(State);
                        break;

                }

            }
        });

    }

    public void CheckGpsStatus(){

        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    /**
     * Checks if the map is ready (which depends on whether the Google Play services APK is
     * available. This should be called prior to calling any methods on GoogleMap.
     */
    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        final View myContentsView = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.title));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
            tvSnippet.setText(marker.getSnippet());


            return myContentsView;
        }
    }
}
